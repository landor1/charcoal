--[[
Charcoal
========

Copyright (c) 2012 Gal Buki <torusJKL@gmail.com>

Version: 0.2

Depends: default

License for code:
GPLv2 or later
(http://www.gnu.org/licenses/gpl-2.0.html)

License for texture:
Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
(http://creativecommons.org/licenses/by-sa/3.0/)
--]]

local modname = "charcoal"

-- Used for localization
local S = minetest.get_translator("charcoal")


--
-- Crafting definition
--

minetest.register_craft({
        output = 'default:torch 4',
        recipe = {
                {'charcoal:charcoal_lump'},
                {'default:stick'},
        }
})

minetest.register_craft({
        type = "fuel",
        recipe = "charcoal:charcoal_lump",
        burntime = 40,
})

--
-- Cooking recipes
--

minetest.register_craft({
        type = "cooking",
        output = "charcoal:charcoal_lump",
        recipe = "group:tree",
})

--
-- Crafting items
--

minetest.register_craftitem("charcoal:charcoal_lump", {
        description = S("Lump of charcoal"),
        inventory_image = "charcoal_lump.png",
})

--Charcoal Block

minetest.register_node("charcoal:charcoal_block", {
	description = "Charcoal Block",
	tiles = {"charcoal_charcoal_block.png"},
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	output = "charcoal:charcoal_block",
	recipe = {
		{"charcoal:charcoal_lump", "charcoal:charcoal_lump",
			"charcoal:charcoal_lump"},
		{"charcoal:charcoal_lump", "charcoal:charcoal_lump",
			"charcoal:charcoal_lump"},
		{"charcoal:charcoal_lump", "charcoal:charcoal_lump",
			"charcoal:charcoal_lump"},
	}
})

minetest.register_craft({
	type = "fuel",
	recipe = "charcoal:charcoal_block",
	burntime = 370,
})

minetest.register_craft({
	output = "charcoal:charcoal_lump 9",
	recipe = {
		{"charcoal:charcoal_block"},
	}
})
